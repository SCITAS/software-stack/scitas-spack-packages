# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)
import platform
from spack.package import *


class Cusparselt(CudaPackage):
    """cuSPARSELt: A High-Performance CUDA Library for Sparse Matrix-Matrix Multiplication"""

    homepage = "https://docs.nvidia.com/cuda/cusparselt/index.html"

    _versions = {
        "0.6.2.3": {
            "Linux-x86_64": "81f649b786869e8c10db08b23a919e0fd223a4d5001e0a1432d392143370729d",
        },
    }

    for ver, packages in _versions.items():
        key = "{0}-{1}".format(platform.system(), platform.machine())
        pkg = packages.get(key)
        ver = ver

        if pkg:
            version(ver, sha256=pkg)

    depends_on("cuda@12:", when="@0.5:")

    def url_for_version(self, version):
        # Get the system and machine arch for building the file path
        sys = "{0}-{1}".format(platform.system(), platform.machine())
        # Munge it to match Nvidia's naming scheme
        sys_key = sys.lower()
        sys_key = sys_key.replace("aarch64", "sbsa")

        url = "https://developer.download.nvidia.com/compute/cusparselt/redist/libcusparse_lt/{0}/libcusparse_lt-{0}-{1}-archive.tar.xz"
        return url.format(sys_key, version)

    def install(self, spec, prefix):
        install_tree(".", prefix)

