# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class StdCompat(CMakePackage):
    """LibStdCompat is a set of compatibility headers for C++ 14, 17, and 20 for
    C++11."""

    homepage = "https://github.com/robertu94/std_compat"
    url = "https://github.com/robertu94/std_compat.git"

    version("0.0.21", git="https://github.com/robertu94/std_compat.git",
            branch="0.0.21")

    depends_on("cmake@3.13:", type=['build'])

    def cmake_args(self):
        args = [
            "BUILD_DOCS=OFF",
            "BUILD_TESTING=OFF"
        ]
        return args
