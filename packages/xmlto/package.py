# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class Xmlto(AutotoolsPackage):
    """Utility xmlto is a simple shell script for converting XML files to various
    formats. It serves as easy to use command line frontend to make fine output
    without remembering many long options and searching for the syntax of the
    backends."""

    homepage = "https://pagure.io/xmlto"
    url = "https://pagure.io/xmlto/archive/0.0.29/xmlto-0.0.29.tar.gz"

    license("GPL-2.0-or-later")

    version("0.0.29", sha256="40504db68718385a4eaa9154a28f59e51e59d006d1aa14f5bc9d6fded1d6017a")
    version("0.0.28", sha256="2f986b7c9a0e9ac6728147668e776d405465284e13c74d4146c9cbc51fd8aad3",
            url="https://releases.pagure.org/xmlto/xmlto-0.0.28.tar.gz")

    # FIXME: missing a lot of dependencies
    depends_on("docbook-xsl", type=("build", "run"))
    depends_on("libxml2", type=("build", "run"))  # xmllint
    depends_on("libxslt", type=("build", "run"))  # xsltconf
    depends_on("util-linux", type=("build", "run"))  # getopt with support for longopts
    depends_on("flex", type=("build"))

    depends_on("docbook-xml", type="run")

    with when('@0.0.29'):
        depends_on("autoconf", type="build")
        depends_on("automake", type="build")
        depends_on("libtool", type="build")


    with when('@0.0.28'):
        patch(
            "https://src.fedoraproject.org/rpms/xmlto/raw/rawhide/f/xmlto-c99-1.patch",
            sha256="056c8bebc25d8d1488cc6a3724e2bcafc0e5e0df5c50080559cdef99bd377839"
        )
        patch(
            "https://src.fedoraproject.org/rpms/xmlto/raw/rawhide/f/xmlto-c99-2.patch",
            sha256="50e39b1810bbf22a1d67944086c5681bcd58b8c325dfb251d56ac15d088fc17a"
        )
