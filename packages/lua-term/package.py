# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class LuaTerm(LuaPackage):
    """lua-term is a Lua module for manipulating a terminal."""

    homepage = "https://github.com/hoelzro/lua-term"
    url = "https://github.com/hoelzro/lua-term/archive/refs/tags/0.08.tar.gz"

    license("MIT", checked_by="nrichart")

    version("0.08", sha256="8ff94f390ea9d98c734699373ca3b0ce500d651b2ab1cb8d7d2336fc5b79cded")
